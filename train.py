import os
import time
import pprint

from options.train_options import TrainOptions

from models.models import create_model
from data.unaligned_data_loader import UnalignedDataLoader

from util.visualizer import Visualizer

def main():
    opt = TrainOptions().parse()

    data_loader = UnalignedDataLoader(opt)
    dataset = data_loader.load_data()
    dataset_size = len(data_loader)
    print('DataLoader: %s, #training images = %d' % (data_loader.name(), dataset_size))

    model = create_model(opt)
    visualizer = Visualizer(opt)

    total_steps = 0
    history = []
    start_time = time.time()
    for epoch in range(1, opt.niter + opt.niter_decay + 1):
        epoch_start_time = time.time()
        for i, data in enumerate(dataset):
            iter_start_time = time.time()
            total_steps += opt.batchSize
            epoch_iter = total_steps - dataset_size * (epoch - 1)
            
            imgs, errors = model.update(data)
            errors = model.error_dict(*errors)
            history.append(errors)

            if total_steps % opt.display_freq == 0:
                visualizer.display_current_results(model.output_dict(*imgs), epoch)

            if total_steps % opt.print_freq == 0:
                visualizer.print_current_errors(epoch, epoch_iter, errors, iter_start_time)

                iters_per_sec = (total_steps/opt.batchSize) / (time.time()-start_time)
                finish_time = ((opt.niter + opt.niter_decay)*dataset_size - total_steps) / opt.batchSize / iters_per_sec
                days, rest_mins = finish_time // 86400, finish_time%86400//60
                print('{} iters/s, finish in {} days, {} hours, {} minutes'.format(iters_per_sec, days, rest_mins//60, rest_mins%60), end='\r')
                
                if opt.display_id > 0:
                    visualizer.plot_current_errors(epoch, float(epoch_iter)/dataset_size, opt, errors)

            if total_steps % opt.save_latest_freq == 0:
                print('saving the latest model (epoch %d, total_steps %d)' % (epoch, total_steps))
                model.save('latest')

        if epoch % opt.save_epoch_freq == 0:
            print('saving the model at the end of epoch %d, iters %d' % (epoch, total_steps))
            model.save('latest')
            model.save(epoch)

            with open(os.path.join(model.save_dir, 'log.txt'), 'w') as f:
                f.write(pprint.pformat(history))

        print('End of epoch %d / %d \t Time Taken: %d sec' % (epoch, opt.niter + opt.niter_decay, time.time() - epoch_start_time))

        if epoch > opt.niter:
            model.update_learning_rate()

if __name__ == '__main__':
    main()