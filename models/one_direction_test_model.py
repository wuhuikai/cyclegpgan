from collections import OrderedDict

from torch.autograd import Variable

import util.util as util

from .base_model import BaseModel

from . import networks

class OneDirectionTestModel(BaseModel):
    def __init__(self, opt):
        super(OneDirectionTestModel, self).__init__(opt)
        assert(not self.isTrain)

        AtoB = opt.which_direction == 'AtoB'
        self.data_key = 'A' if AtoB else 'B'
        self.path_key = 'A_paths' if AtoB else 'B_paths'

        self.netG_A = networks.define_G(opt.input_nc, opt.output_nc,
                                        opt.ngf, opt.which_model_netG,
                                        opt.norm, opt.use_dropout,
                                        self.gpu_ids, opt.bias, opt.padding)
        self.load_network(self.netG_A, 'G_A' if AtoB else 'G_B', opt.which_epoch)

        print('---------- Networks initialized -------------')
        networks.print_network(self.netG_A)
        print('-----------------------------------------------')

    def name(self):
        return 'OneDirectionTestModel'

    def test(self, input):
        real_A_var = Variable(input[self.data_key], volatile=True)
        fake_B_var = self.netG_A.forward(real_A_var)

        return self.output_dict(real_A_var, fake_B_var), input[self.path_key]

    def output_dict(self, real_A_var, fake_B_var):
        real_A = util.tensor2im(real_A_var.data)
        fake_B = util.tensor2im(fake_B_var.data)
        return OrderedDict([('real_A', real_A), ('fake_B', fake_B)])