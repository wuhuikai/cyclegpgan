import os
import sys
import itertools
from collections import OrderedDict

import numpy as np

import torch
from torch.autograd import Variable

import util.util as util

from util.image_pool import ImagePool

from .base_model import BaseModel

from . import networks

class CycleGANModel(BaseModel):
    def __init__(self, opt):
        super(CycleGANModel, self).__init__(opt)

        # load/define networks
        # The naming conversion is different from those used in the paper
        # Code (paper): G_A (G), G_B (F), D_A (D_Y), D_B (D_X)
        self.netG_A = networks.define_G(opt.input_nc, opt.output_nc,
                                     opt.ngf, opt.which_model_netG, opt.norm, opt.use_dropout, opt.gpu_ids, opt.bias, opt.padding)
        self.netG_B = networks.define_G(opt.output_nc, opt.input_nc,
                                     opt.ngf, opt.which_model_netG, opt.norm, opt.use_dropout, opt.gpu_ids, opt.bias, opt.padding)

        if opt.isTrain:
            self.netD_A = networks.define_D(opt.output_nc, opt.ndf,
                                         opt.which_model_netD, opt.norm,
                                         opt.n_layers_D, opt.gpu_ids, opt.bias)
            self.netD_B = networks.define_D(opt.input_nc, opt.ndf,
                                         opt.which_model_netD, opt.norm,
                                         opt.n_layers_D, opt.gpu_ids, opt.bias)
        
        if not opt.isTrain or opt.continue_train:
            which_epoch = opt.which_epoch
            self.load_network(self.netG_A, 'G_A', which_epoch)
            self.load_network(self.netG_B, 'G_B', which_epoch)
            if opt.isTrain:
                self.load_network(self.netD_A, 'D_A', which_epoch)
                self.load_network(self.netD_B, 'D_B', which_epoch)

        if opt.isTrain:
            self.old_lr = opt.lr
            self.fake_A_pool = ImagePool(opt.pool_size)
            self.fake_B_pool = ImagePool(opt.pool_size)
            # define loss functions
            self.criterionGAN = networks.GANLoss(0.9, 0.0, tensor=self.Tensor, gpu=len(opt.gpu_ids) > 0)
            self.criterionCycle = torch.nn.L1Loss()
            self.criterionIdt = torch.nn.L1Loss()
            # initialize optimizers
            self.optimizer_G = torch.optim.Adam(itertools.chain(self.netG_A.parameters(), self.netG_B.parameters()),
                                                lr=opt.lr, betas=(opt.beta1, 0.999))
            self.optimizer_D_A = torch.optim.Adam(self.netD_A.parameters(),
                                                lr=opt.lr, betas=(opt.beta1, 0.999))
            self.optimizer_D_B = torch.optim.Adam(self.netD_B.parameters(),
                                                lr=opt.lr, betas=(opt.beta1, 0.999))

            print('---------- Networks initialized -------------')
            networks.print_network(self.netG_A)
            networks.print_network(self.netG_B)
            networks.print_network(self.netD_A)
            networks.print_network(self.netD_B)
            print('-----------------------------------------------')

    def name(self):
        return 'CycleGANModel'

    def test(self, input):
        real_A = Variable(input['A'], volatile=True)
        fake_B = self.netG_A.forward(real_A)
        rec_A = self.netG_B.forward(fake_B)

        real_B = Variable(input['B'], volatile=True)
        fake_A = self.netG_B.forward(real_B)
        rec_B  = self.netG_A.forward(fake_A)

        return self.output_dict(real_A, real_B, fake_A, fake_B, rec_A, rec_B), input['A_paths']

    def backward_D_basic(self, netD, real, fake):
        # Real
        pred_real = netD.forward(real)
        loss_D_real = self.criterionGAN(pred_real, True)
        # Fake
        pred_fake = netD.forward(fake.detach())
        loss_D_fake = self.criterionGAN(pred_fake, False)
        # Combined loss
        loss_D = (loss_D_real + loss_D_fake) * 0.5
        # backward
        loss_D.backward()
        return loss_D

    def backward_D_A(self, real_B, fake_B_):
        fake_B = self.fake_B_pool.query(fake_B_)
        return self.backward_D_basic(self.netD_A, real_B, fake_B)

    def backward_D_B(self, real_A, fake_A_):
        fake_A = self.fake_A_pool.query(fake_A_)
        return  self.backward_D_basic(self.netD_B, real_A, fake_A)

    def backward_G(self, real_A, real_B):
        lambda_idt = self.opt.identity
        lambda_A = self.opt.lambda_A
        lambda_B = self.opt.lambda_B
        # Identity loss
        if lambda_idt > 0:
            # G_A should be identity if real_B is fed.
            idt_A = self.netG_A.forward(real_B)
            loss_idt_A = self.criterionIdt(idt_A, real_B) * lambda_B * lambda_idt
            # G_B should be identity if real_A is fed.
            idt_B = self.netG_B.forward(real_A)
            loss_idt_B = self.criterionIdt(idt_B, real_A) * lambda_A * lambda_idt
        else:
            idt_A, idt_B = None, None
            loss_idt_A, loss_idt_B = 0, 0

        # GAN loss
        # D_A(G_A(A))
        fake_B = self.netG_A.forward(real_A)
        pred_fake = self.netD_A.forward(fake_B)
        loss_G_A = self.criterionGAN(pred_fake, True)
        # D_B(G_B(B))
        fake_A = self.netG_B.forward(real_B)
        pred_fake = self.netD_B.forward(fake_A)
        loss_G_B = self.criterionGAN(pred_fake, True)
        # Forward cycle loss
        rec_A = self.netG_B.forward(fake_B)
        loss_cycle_A = self.criterionCycle(rec_A, real_A) * lambda_A
        # Backward cycle loss
        rec_B = self.netG_A.forward(fake_A)
        loss_cycle_B = self.criterionCycle(rec_B, real_B) * lambda_B
        # combined loss
        loss_G = loss_G_A + loss_G_B + loss_cycle_A + loss_cycle_B + loss_idt_A + loss_idt_B
        loss_G.backward()

        return idt_A, idt_B, fake_A, fake_B, rec_A, rec_B,loss_idt_A, loss_idt_B, loss_G_A, loss_G_B, loss_cycle_A, loss_cycle_B, loss_G

    def update(self, input):
        real_A = Variable(input['A'])
        real_B = Variable(input['B'])

        # G_A and G_B
        self.optimizer_G.zero_grad()
        idt_A, idt_B, fake_A, fake_B, rec_A, rec_B,loss_idt_A, \
            loss_idt_B, loss_G_A, loss_G_B, loss_cycle_A, loss_cycle_B, loss_G = self.backward_G(real_A, real_B)
        self.optimizer_G.step()
        # D_A
        self.optimizer_D_A.zero_grad()
        loss_D_A = self.backward_D_A(real_B, fake_B)
        self.optimizer_D_A.step()
        # D_B
        self.optimizer_D_B.zero_grad()
        loss_D_B = self.backward_D_B(real_A, fake_A)
        self.optimizer_D_B.step()

        return (real_A, real_B, fake_A, fake_B, rec_A, rec_B, idt_A, idt_B), \
                (loss_D_A, loss_D_B, loss_G_A, loss_G_B, loss_cycle_A, loss_cycle_B, loss_idt_A, loss_idt_B)

    def output_dict(self, real_A, real_B, fake_A, fake_B, rec_A, rec_B, idt_A=None, idt_B=None):
        real_A = util.tensor2im(real_A.data)
        real_B = util.tensor2im(real_B.data)

        fake_A = util.tensor2im(fake_A.data)
        fake_B = util.tensor2im(fake_B.data)

        rec_A  = util.tensor2im(rec_A.data)
        rec_B  = util.tensor2im(rec_B.data)

        if idt_A is None or idt_B is None:
            return OrderedDict([('real_A', real_A), ('fake_B', fake_B), ('rec_A', rec_A),
                                ('real_B', real_B), ('fake_A', fake_A), ('rec_B', rec_B)])
        else:
            idt_A = util.tensor2im(idt_A.data)
            idt_B = util.tensor2im(idt_B.data)
            return OrderedDict([('real_A', real_A), ('fake_B', fake_B), ('rec_A', rec_A), ('idt_B', idt_B),
                                ('real_B', real_B), ('fake_A', fake_A), ('rec_B', rec_B), ('idt_A', idt_A)])

    def error_dict(self, loss_D_A, loss_D_B, loss_G_A, loss_G_B, loss_cycle_A, loss_cycle_B, loss_idt_A, loss_idt_B):
        D_A = loss_D_A.data[0]
        D_B = loss_D_B.data[0]

        G_A = loss_G_A.data[0]
        G_B = loss_G_B.data[0]

        Cyc_A = loss_cycle_A.data[0]
        Cyc_B = loss_cycle_B.data[0]

        if self.opt.identity <= 0:
            return OrderedDict([('D_A', D_A), ('G_A', G_A), ('Cyc_A', Cyc_A),
                                ('D_B', D_B), ('G_B', G_B), ('Cyc_B', Cyc_B)])
        else:
            idt_A = loss_idt_A.data[0]
            idt_B = loss_idt_B.data[0]
            return OrderedDict([('D_A', D_A), ('G_A', G_A), ('Cyc_A', Cyc_A), ('idt_A', idt_A),
                                ('D_B', D_B), ('G_B', G_B), ('Cyc_B', Cyc_B), ('idt_B', idt_B)])

    def save(self, label):
        self.save_network(self.netG_A, 'G_A', label, self.opt.gpu_ids)
        self.save_network(self.netD_A, 'D_A', label, self.opt.gpu_ids)
        self.save_network(self.netG_B, 'G_B', label, self.opt.gpu_ids)
        self.save_network(self.netD_B, 'D_B', label, self.opt.gpu_ids)

    def update_learning_rate(self):
        lrd = self.opt.lr / self.opt.niter_decay
        lr = self.old_lr - lrd
        for param_group in self.optimizer_D_A.param_groups:
            param_group['lr'] = lr
        for param_group in self.optimizer_D_B.param_groups:
            param_group['lr'] = lr
        for param_group in self.optimizer_G.param_groups:
            param_group['lr'] = lr

        print('update learning rate: %f -> %f' % (self.old_lr, lr))
        self.old_lr = lr