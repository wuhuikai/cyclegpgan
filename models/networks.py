import numpy as np

import torch
import torch.nn as nn
from torch.autograd import Variable

###############################################################################
# Functions
###############################################################################
def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('Norm2d') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)

def define_G(input_nc, output_nc, ngf, which_model_netG, norm, use_dropout=False, gpu_ids=[], bias=False, padding='reflect'):
    if norm == 'batch':
        norm_layer = nn.BatchNorm2d
    elif norm == 'instance':
        norm_layer = nn.InstanceNorm2d
    else:
        print('normalization layer [%s] is not found' % norm)

    if which_model_netG == 'resnet_9blocks':
        netG = ResnetGenerator(input_nc, output_nc, ngf, norm_layer, use_dropout=use_dropout, n_blocks=9, gpu_ids=gpu_ids, bias=bias, padding=padding)
    elif which_model_netG == 'resnet_6blocks':
        netG = ResnetGenerator(input_nc, output_nc, ngf, norm_layer, use_dropout=use_dropout, n_blocks=6, gpu_ids=gpu_ids, bias=bias, padding=padding)
    else:
        print('Generator model name [%s] is not recognized' % which_model_netG)
    
    if len(gpu_ids) > 0:
        assert(torch.cuda.is_available())
        netG.cuda(device_id=gpu_ids[0])
    
    netG.apply(weights_init)
    
    return netG

def define_D(input_nc, ndf, which_model_netD, norm, n_layers_D=3, gpu_ids=[], bias=False):
    if which_model_netD == 'basic':
        netD = define_D(input_nc, ndf, 'n_layers', norm, gpu_ids=gpu_ids, bias=bias)
    elif which_model_netD == 'n_layers':
        netD = NLayerDiscriminator(input_nc, ndf, n_layers_D, norm, gpu_ids=gpu_ids, bias=bias)
    else:
        print('Discriminator model name [%s] is not recognized' % which_model_netD)

    if len(gpu_ids) > 0:
        assert(torch.cuda.is_available())
        netD.cuda(device_id=gpu_ids[0])

    netD.apply(weights_init)
    
    return netD

def print_network(net):
    num_params = 0
    for param in net.parameters():
        num_params += param.numel()
    print(net)
    print('Total number of parameters: %d' % num_params)

##############################################################################
# Classes
##############################################################################
# Defines the GAN loss which uses LSGAN.
# When LSGAN is used, it is basically same as MSELoss,
# but it abstracts away the need to create the target label tensor
# that has the same size as the input
class GANLoss(nn.Module):
    def __init__(self, target_real_label=1.0, target_fake_label=0.0, tensor=torch.FloatTensor, gpu=True):
        super(GANLoss, self).__init__()
        self.real_label_var = Variable(tensor([1]).fill_(target_real_label), requires_grad=False)
        self.fake_label_var = Variable(tensor([1]).fill_(target_fake_label), requires_grad=False)
        self.loss = nn.MSELoss()
        self.gpu = gpu

    def __call__(self, input, target_is_real):
        label = self.real_label_var if target_is_real else self.fake_label_var
        if self.gpu:
            label.data = label.data.cuda(input.data.get_device())

        return self.loss(input, label.expand_as(input.data))

# Defines the generator that consists of Resnet blocks between a few
# downsampling/upsampling operations.
# Code and idea originally from Justin Johnson's architecture.
# https://github.com/jcjohnson/fast-neural-style/
class ResnetGenerator(nn.Module):
    def __init__(self, input_nc, output_nc, ngf=64, norm_layer=nn.BatchNorm2d, use_dropout=False, n_blocks=6, gpu_ids=[], bias=False, padding='reflect'):
        super(ResnetGenerator, self).__init__()
        
        assert(n_blocks >= 0)
        self.gpu_ids = gpu_ids

        if padding == 'reflect':
            model, p = [nn.ReflectionPad2d(3)], 0
        else:
            model, p = [], 3

        model += [nn.Conv2d(input_nc, ngf, kernel_size=7, padding=p, bias=bias),
                  norm_layer(ngf, affine=True),
                  nn.ReLU(True)]

        n_downsampling = 2
        for i in range(n_downsampling):
            mult = 2**i
            model += [nn.Conv2d(ngf * mult, ngf * mult * 2, kernel_size=3, stride=2, padding=1, bias=bias),
                      norm_layer(ngf * mult * 2, affine=True),
                      nn.ReLU(True)]

        mult = 2**n_downsampling
        for i in range(n_blocks):
            model += [ResnetBlock(ngf * mult, norm_layer=norm_layer, use_dropout=use_dropout, bias=bias, padding=padding)]

        for i in range(n_downsampling):
            mult = 2**(n_downsampling - i)
            model += [nn.ConvTranspose2d(ngf * mult, int(ngf * mult / 2), kernel_size=3, stride=2, padding=1, output_padding=1, bias=bias),
                      norm_layer(int(ngf * mult / 2), affine=True),
                      nn.ReLU(True)]

        if padding == 'reflect':
            model += [nn.ReflectionPad2d(3)]
            p = 0
        else:
            p = 3
        
        model += [nn.Conv2d(ngf, output_nc, kernel_size=7, padding=p, bias=bias)]
        model += [nn.Tanh()]

        self.model = nn.Sequential(*model)

    def forward(self, input):
        if self.gpu_ids and isinstance(input.data, torch.cuda.FloatTensor):
            return nn.parallel.data_parallel(self.model, input, self.gpu_ids)
        else:
            return self.model(input)

# Define a resnet block
class ResnetBlock(nn.Module):
    def __init__(self, dim, norm_layer, use_dropout, bias=False, padding='reflect'):
        super(ResnetBlock, self).__init__()
        self.conv_block = self.build_conv_block(dim, norm_layer, use_dropout, bias, padding)

    def build_conv_block(self, dim, norm_layer, use_dropout, bias, padding):
        if padding == 'reflect':
            conv_block, p = [nn.ReflectionPad2d(1)], 0
        else:
            conv_block, p = [], 1
        
        conv_block += [nn.Conv2d(dim, dim, kernel_size=3, padding=p, bias=bias),
                       norm_layer(dim, affine=True),
                       nn.ReLU(True)]
        
        if use_dropout:
            conv_block += [nn.Dropout(0.5)]

        if padding == 'reflect':
            conv_block += [nn.ReflectionPad2d(1)]
            p = 0
        else:
            p = 1
        conv_block += [nn.Conv2d(dim, dim, kernel_size=3, padding=p, bias=bias),
                       norm_layer(dim, affine=True)]

        return nn.Sequential(*conv_block)

    def forward(self, x):
        out = x + self.conv_block(x)
        return out

# Defines the PatchGAN discriminator with the specified arguments.
class NLayerDiscriminator(nn.Module):
    def __init__(self, input_nc, ndf=64, n_layers=3, norm='batch', gpu_ids=[], bias=False):
        super(NLayerDiscriminator, self).__init__()
        
        self.gpu_ids = gpu_ids

        if norm == 'batch':
            norm_layer = nn.BatchNorm2d
        elif norm == 'instance':
            norm_layer = nn.InstanceNorm2d
        else:
            print('normalization layer [%s] is not found' % norm)

        kw = 4
        padw = int(np.ceil((kw-1)/2))
        sequence = [
            nn.Conv2d(input_nc, ndf, kernel_size=kw, stride=2, padding=padw, bias=bias),
            nn.LeakyReLU(0.2, True)
        ]

        nf_mult, nf_mult_prev = 1, 1
        for n in range(1, n_layers):
            nf_mult_prev = nf_mult
            nf_mult = min(2**n, 8)
            sequence += [
                nn.Conv2d(ndf * nf_mult_prev, ndf * nf_mult, kernel_size=kw, stride=2, padding=padw, bias=bias),
                norm_layer(ndf * nf_mult, affine=True),
                nn.LeakyReLU(0.2, True)
            ]

        nf_mult_prev = nf_mult
        nf_mult = min(2**n_layers, 8)
        sequence += [
            nn.Conv2d(ndf * nf_mult_prev, ndf * nf_mult, kernel_size=kw, stride=1, padding=padw, bias=bias),
            norm_layer(ndf * nf_mult, affine=True),
            nn.LeakyReLU(0.2, True)
        ]

        sequence += [nn.Conv2d(ndf * nf_mult, 1, kernel_size=kw, stride=1, padding=padw, bias=bias)]

        self.model = nn.Sequential(*sequence)

    def forward(self, input):
        if len(self.gpu_ids) and isinstance(input.data, torch.cuda.FloatTensor):
            return nn.parallel.data_parallel(self.model, input, self.gpu_ids)
        else:
            return self.model(input)