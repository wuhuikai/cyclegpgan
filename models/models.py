def create_model(opt):
    if opt.model == 'cycle_gan':
        from .cycle_gan_model import CycleGANModel
        model = CycleGANModel(opt)
    elif opt.model == 'one_direction_test':
        from .one_direction_test_model import OneDirectionTestModel
        model = OneDirectionTestModel(opt)
    else:
        raise ValueError("Model [%s] not recognized." % opt.model)
    print("model [%s] was created" % (model.name()))
    return model