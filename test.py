import os
import time

from util import html, util
from util.visualizer import Visualizer

from models.models import create_model
from options.test_options import TestOptions
from data.unaligned_data_loader import UnalignedDataLoader

def main():
	opt = TestOptions().parse()
	opt.nThreads = 1   # test code only supports nThreads=1
	opt.batchSize = 1  # test code only supports batchSize=1
	opt.serial_batches = True # no shuffle
	opt.no_flip = True
	opt.loadSize = opt.fineSize

	data_loader = UnalignedDataLoader(opt)
	dataset = data_loader.load_data()
	model = create_model(opt)
	visualizer = Visualizer(opt)
	# create website
	web_dir = os.path.join(opt.results_dir, opt.name, '%s_%s' % (opt.phase, opt.which_epoch))
	img_folder = 'images'
	webpage = html.HTML(web_dir, img_folder, 'Experiment = %s, Phase = %s, Epoch = %s' % (opt.name, opt.phase, opt.which_epoch))
	# test
	for i, data in enumerate(dataset):
	    if i >= opt.how_many:
	        break
	    visuals, img_path = model.test(data)
	    print('process image... %s' % img_path)
	    visualizer.save_images(webpage, visuals, img_path)

	webpage.save()

if __name__ == '__main__':
	main()