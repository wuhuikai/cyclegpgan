import torch
import torch.utils.data
import torchvision.transforms as transforms

from data.image_folder import ImageFolder

class PairedData(object):
    def __init__(self, data_loader_A, data_loader_B, max_dataset_size, gpu_ids):
        self.data_loader_A = data_loader_A
        self.data_loader_B = data_loader_B
        self.max_dataset_size = max_dataset_size
        self.type = torch.cuda.FloatTensor if gpu_ids else torch.Tensor
        self.gpu_id = gpu_ids[0] if gpu_ids else -1

    def __iter__(self):
        self.stop_A = False
        self.stop_B = False
        self.data_loader_A_iter = iter(self.data_loader_A)
        self.data_loader_B_iter = iter(self.data_loader_B)
        self.iter = 0
        return self

    def __next__(self):
        A, A_paths = None, None
        B, B_paths = None, None
        try:
            A, A_paths = next(self.data_loader_A_iter)
        except StopIteration:
            if A is None or A_paths is None:
                self.stop_A = True
                self.data_loader_A_iter = iter(self.data_loader_A)
                A, A_paths = next(self.data_loader_A_iter)

        try:
            B, B_paths = next(self.data_loader_B_iter)
        except StopIteration:
            if B is None or B_paths is None:
                self.stop_B = True
                self.data_loader_B_iter = iter(self.data_loader_B)
                B, B_paths = next(self.data_loader_B_iter)

        if (self.stop_A and self.stop_B) or self.iter > self.max_dataset_size:
            raise StopIteration()
        else:
            self.iter += 1
            A, B = A.type(self.type), B.type(self.type)
            if self.gpu_id >= 0:
                A, B = A.cuda(self.gpu_id), B.cuda(self.gpu_id)

            return {'A': A, 'A_paths': A_paths,
                    'B': B, 'B_paths': B_paths}

class UnalignedDataLoader(object):
    def __init__(self, opt):
        
        transform_actions = []
        if not opt.no_flip:
            transform_actions.append(transforms.RandomHorizontalFlip())
        transform_actions.append(transforms.Scale(opt.loadSize))
        if opt.isTrain:
            transform_actions.append(transforms.RandomCrop(opt.fineSize))
        transform_actions += [ transforms.ToTensor(),
                               transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)) ]
        
        transform = transforms.Compose(transform_actions)

        # Dataset A
        dataset_A = ImageFolder(root=opt.dataroot + '/' + opt.phase + 'A',
                                transform=transform, return_paths=True)
        data_loader_A = torch.utils.data.DataLoader(
                            dataset_A,
                            batch_size=opt.batchSize,
                            shuffle=not opt.serial_batches,
                            num_workers=int(opt.nThreads)
                        )

        # Dataset B
        dataset_B = ImageFolder(root=opt.dataroot + '/' + opt.phase + 'B',
                                transform=transform, return_paths=True)
        data_loader_B = torch.utils.data.DataLoader(
                            dataset_B,
                            batch_size=opt.batchSize,
                            shuffle=not opt.serial_batches,
                            num_workers=int(opt.nThreads)
                        )
        
        self.length = min(max(len(dataset_A), len(dataset_B)), opt.max_dataset_size)
        self.paired_data = PairedData(data_loader_A, data_loader_B, opt.max_dataset_size, opt.gpu_ids)

    def name(self):
        return 'UnalignedDataLoader'

    def load_data(self):
        return self.paired_data

    def __len__(self):
        return self.length