import os

import dominate
from dominate.tags import *

from . import util

class HTML:
    def __init__(self, web_dir, img_folder, title, reflesh=0):
        self.title = title
        
        self.img_folder = img_folder

        self.web_dir = web_dir
        self.img_dir = os.path.join(self.web_dir, self.img_folder)
        util.mkdirs([self.web_dir, self.img_dir])

        self.doc = dominate.document(title=title)
        if reflesh > 0:
            with self.doc.head:
                meta(http_equiv="reflesh", content=str(reflesh))

    def get_image_dir(self):
        return self.img_dir

    def add_header(self, str):
        with self.doc:
            h3(str)

    def add_table(self, border=1):
        t = table(border=border, style="table-layout: fixed;")
        self.doc.add(t)
        return t

    def add_images(self, ims, txts, links, width=400):
        t = self.add_table()
        with t:
            with tr():
                for im, txt, link in zip(ims, txts, links):
                    with td(style="word-wrap: break-word;", halign="center", valign="top"):
                        with p():
                            with a(href=os.path.join(self.img_folder, link)):
                                img(style="width:%dpx" % width, src=os.path.join(self.img_folder, im))
                            br()
                            p(txt)

    def save(self):
        html_file = '%s/index.html' % self.web_dir
        f = open(html_file, 'wt')
        f.write(self.doc.render())
        f.close()